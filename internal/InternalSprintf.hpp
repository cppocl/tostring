/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TOSTRING_INTERNAL_INTERNALSPRINTF_HPP
#define OCL_GUARD_TOSTRING_INTERNAL_INTERNALSPRINTF_HPP

#include "InternalPrintfFormat.hpp"
#include <cstdio>
#include <cwchar>

namespace ocl
{

template<typename CharType, typename NumericType, typename SizeType = std::size_t>
struct InternalSprintf
{
    typedef char char_type;
    typedef NumericType numeric_type;
    typedef SizeType size_type;

    static constexpr char_type const* GetFormatString() noexcept
    {
        return InternalPrintFormat<char_type, numeric_type>::GetFormatString();
    }

    /// Store the converted value into the string, specifying the size of the string in characters.
    static bool Sprintf(char_type* str, size_type str_size, numeric_type value)
    {
        bool success;

        if ((str != nullptr) && (str_size > 0))
        {
            int ret = ::snprintf(str, str_size, GetFormatString(), value);
            success = (ret > 0) && (static_cast<size_type>(ret) <= str_size);
        }
        else
            success = false;

        return success;
    }
};

template<typename NumericType, typename SizeType>
struct InternalSprintf<wchar_t, NumericType, SizeType>
{
    typedef wchar_t char_type;
    typedef NumericType numeric_type;
    typedef SizeType size_type;

    static constexpr char_type const* GetFormatString() noexcept
    {
        return InternalPrintFormat<char_type, numeric_type>::GetFormatString();
    }

    /// Store the converted value into the string, specifying the size of the string in characters.
    static bool Sprintf(char_type* str, size_type str_size, numeric_type value)
    {
        bool success;

        if ((str != nullptr) && (str_size > 0))
        {
            int ret = ::swprintf(str, str_size, GetFormatString(), value);
            success = (ret > 0) && (static_cast<size_type>(ret) <= str_size);
        }
        else
            success = false;

        return success;
    }
};

} // namespace ocl

#endif // OCL_GUARD_TOSTRING_INTERNAL_INTERNALSPRINTF_HPP
