/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_TOSTRING_INTERNAL_INTERNALPRINTFFORMAT_HPP
#define OCL_GUARD_TOSTRING_INTERNAL_INTERNALPRINTFFORMAT_HPP

#include <cstddef>

namespace ocl
{

/// CharType will be char or wchar_t.
/// NumericType represents integer or floating point type.
template<typename CharType, typename NumericType>
struct InternalPrintFormat;

template<>
struct InternalPrintFormat<char, char>
{
    typedef char char_type;    // char type for target conversion.
    typedef char numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef char promotion_type;

    static char_type GetFormatChar() noexcept { return 'c'; }
    static constexpr char_type const* GetLengthString() noexcept { return ""; }
    static constexpr char_type const* GetFormatString() noexcept { return "%c"; }
};

template<>
struct InternalPrintFormat<wchar_t, char>
{
    typedef wchar_t char_type; // char type for target conversion.
    typedef char numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef char promotion_type;

    static char_type GetFormatChar() noexcept { return L'c'; }
    static constexpr char_type const* GetLengthString() noexcept { return L""; }
    static constexpr char_type const* GetFormatString() noexcept { return L"%c"; }
};

template<>
struct InternalPrintFormat<char, wchar_t>
{
    typedef char char_type;       // char type for target conversion.
    typedef wchar_t numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef char promotion_type;

    static char_type GetFormatChar() noexcept { return 'c'; }
    static constexpr char_type const* GetLengthString() noexcept { return "l"; }
    static constexpr char_type const* GetFormatString() noexcept { return "%lc"; }
};

template<>
struct InternalPrintFormat<wchar_t, wchar_t>
{
    typedef wchar_t char_type;     // char type for target conversion.
    typedef wchar_t numeric_type;  // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef char promotion_type;

    static char_type GetFormatChar() noexcept { return L'c'; }
    static constexpr char_type const* GetLengthString() noexcept { return L""; }
    static constexpr char_type const* GetFormatString() noexcept { return L"%c"; }
};

template<>
struct InternalPrintFormat<char, signed char>
{
    typedef char char_type;           // char type for target conversion.
    typedef signed char numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef signed int promotion_type;

    static char_type GetFormatChar() noexcept { return 'd'; }
    static constexpr char_type const* GetLengthString() noexcept { return ""; }
    static constexpr char_type const* GetFormatString() noexcept { return "%d"; }
};

template<>
struct InternalPrintFormat<wchar_t, signed char>
{
    typedef wchar_t char_type;        // char type for target conversion.
    typedef signed char numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef signed int promotion_type;

    static char_type GetFormatChar() noexcept { return L'd'; }
    static constexpr char_type const* GetLengthString() noexcept { return L""; }
    static constexpr char_type const* GetFormatString() noexcept { return L"%d"; }
};

template<>
struct InternalPrintFormat<char, unsigned char>
{
    typedef char char_type;             // char type for target conversion.
    typedef unsigned char numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned int promotion_type;

    static char_type GetFormatChar() noexcept { return 'u'; }
    static constexpr char_type const* GetLengthString() noexcept { return ""; }
    static constexpr char_type const* GetFormatString() noexcept { return "%u"; }
};

template<>
struct InternalPrintFormat<wchar_t, unsigned char>
{
    typedef wchar_t char_type;          // char type for target conversion.
    typedef unsigned char numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned int promotion_type;

    static char_type GetFormatChar() noexcept { return L'u'; }
    static constexpr char_type const* GetLengthString() noexcept { return L""; }
    static constexpr char_type const* GetFormatString() noexcept { return L"%u"; }
};

template<>
struct InternalPrintFormat<char, signed short>
{
    typedef char char_type;            // char type for target conversion.
    typedef signed short numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef signed int promotion_type;

    static char_type GetFormatChar() noexcept { return 'd'; }
    static constexpr char_type const* GetLengthString() noexcept { return ""; }
    static constexpr char_type const* GetFormatString() noexcept { return "%d"; }
};

template<>
struct InternalPrintFormat<wchar_t, signed short>
{
    typedef wchar_t char_type;         // char type for target conversion.
    typedef signed short numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef signed int promotion_type;

    static char_type GetFormatChar() noexcept { return L'd'; }
    static constexpr char_type const* GetLengthString() noexcept { return L""; }
    static constexpr char_type const* GetFormatString() noexcept { return L"%d"; }
};

template<>
struct InternalPrintFormat<char, unsigned short>
{
    typedef char char_type;              // char type for target conversion.
    typedef unsigned short numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned int promotion_type;

    static char_type GetFormatChar() noexcept { return 'u'; }
    static constexpr char_type const* GetLengthString() noexcept { return ""; }
    static constexpr char_type const* GetFormatString() noexcept { return "%u"; }
};

template<>
struct InternalPrintFormat<wchar_t, unsigned short>
{
    typedef wchar_t char_type;           // char type for target conversion.
    typedef unsigned short numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned int promotion_type;

    static char_type GetFormatChar() noexcept { return L'u'; }
    static constexpr char_type const* GetLengthString() noexcept { return L""; }
    static constexpr char_type const* GetFormatString() noexcept { return L"%u"; }
};

template<>
struct InternalPrintFormat<char, signed int>
{
    typedef char char_type;          // char type for target conversion.
    typedef signed int numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef signed int promotion_type;

    static char_type GetFormatChar() noexcept { return 'd'; }
    static constexpr char_type const* GetLengthString() noexcept { return ""; }
    static constexpr char_type const* GetFormatString() noexcept { return "%d"; }
};

template<>
struct InternalPrintFormat<wchar_t, signed int>
{
    typedef wchar_t char_type;       // char type for target conversion.
    typedef signed int numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef signed int promotion_type;

    static char_type GetFormatChar() noexcept { return L'd'; }
    static constexpr char_type const* GetLengthString() noexcept { return L""; }
    static constexpr char_type const* GetFormatString() noexcept { return L"%d"; }
};

template<>
struct InternalPrintFormat<char, unsigned int>
{
    typedef char char_type;            // char type for target conversion.
    typedef unsigned int numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned int promotion_type;

    static char_type GetFormatChar() noexcept { return 'u'; }
    static constexpr char_type const* GetLengthString() noexcept { return ""; }
    static constexpr char_type const* GetFormatString() noexcept { return "%u"; }
};

template<>
struct InternalPrintFormat<wchar_t, unsigned int>
{
    typedef wchar_t char_type;         // char type for target conversion.
    typedef unsigned int numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned int promotion_type;

    static char_type GetFormatChar() noexcept { return L'u'; }
    static constexpr char_type const* GetLengthString() noexcept { return L""; }
    static constexpr char_type const* GetFormatString() noexcept { return L"%u"; }
};

template<>
struct InternalPrintFormat<char, signed long int>
{
    typedef char char_type;               // char type for target conversion.
    typedef signed long int numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef signed long int promotion_type;

    static char_type GetFormatChar() noexcept { return 'd'; }
    static constexpr char_type const* GetLengthString() noexcept { return "l"; }
    static constexpr char_type const* GetFormatString() noexcept { return "%ld"; }
};

template<>
struct InternalPrintFormat<wchar_t, signed long int>
{
    typedef wchar_t char_type;            // char type for target conversion.
    typedef signed long int numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef signed long int promotion_type;

    static char_type GetFormatChar() noexcept { return L'd'; }
    static constexpr char_type const* GetLengthString() noexcept { return L"l"; }
    static constexpr char_type const* GetFormatString() noexcept { return L"%ld"; }
};

template<>
struct InternalPrintFormat<char, unsigned long int>
{
    typedef char char_type;                 // char type for target conversion.
    typedef unsigned long int numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned long int promotion_type;

    static char_type GetFormatChar() noexcept { return 'u'; }
    static constexpr char_type const* GetLengthString() noexcept { return "l"; }
    static constexpr char_type const* GetFormatString() noexcept { return "%lu"; }
};

template<>
struct InternalPrintFormat<wchar_t, unsigned long int>
{
    typedef wchar_t char_type;              // char type for target conversion.
    typedef unsigned long int numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned long int promotion_type;

    static char_type GetFormatChar() noexcept { return L'u'; }
    static constexpr char_type const* GetLengthString() noexcept { return L"l"; }
    static constexpr char_type const* GetFormatString() noexcept { return L"%lu"; }
};

template<>
struct InternalPrintFormat<char, signed long long int>
{
    typedef char char_type;                    // char type for target conversion.
    typedef signed long long int numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef signed long long int promotion_type;

    static char_type GetFormatChar() noexcept { return 'd'; }
    static constexpr char_type const* GetLengthString() noexcept { return "ll"; }
    static constexpr char_type const* GetFormatString() noexcept { return "%lld"; }
};

template<>
struct InternalPrintFormat<wchar_t, signed long long int>
{
    typedef wchar_t char_type;                 // char type for target conversion.
    typedef signed long long int numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef signed long long int promotion_type;

    static char_type GetFormatChar() noexcept { return L'd'; }
    static constexpr char_type const* GetLengthString() noexcept { return L"ll"; }
    static constexpr char_type const* GetFormatString() noexcept { return L"%lld"; }
};

template<>
struct InternalPrintFormat<char, unsigned long long int>
{
    typedef unsigned long long int numeric_type; // data type being converted to string.
    typedef char char_type;                  // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned long long int promotion_type;

    static char_type GetFormatChar() noexcept { return 'u'; }
    static constexpr char_type const* GetLengthString() noexcept { return "ll"; }
    static constexpr char_type const* GetFormatString() noexcept { return "%llu"; }
};

template<>
struct InternalPrintFormat<wchar_t, unsigned long long int>
{
    typedef unsigned long long int numeric_type; // data type being converted to string.
    typedef wchar_t char_type;               // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef unsigned long long int promotion_type;

    static char_type GetFormatChar() noexcept { return L'u'; }
    static constexpr char_type const* GetLengthString() noexcept { return L"ll"; }
    static constexpr char_type const* GetFormatString() noexcept { return L"%llu"; }
};

template<>
struct InternalPrintFormat<char, float>
{
    typedef char char_type;     // char type for target conversion.
    typedef float numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef double promotion_type;

    static char_type GetFormatChar() noexcept { return 'f'; }
    static constexpr char_type const* GetLengthString() noexcept { return ""; }
    static constexpr char_type const* GetFormatString() noexcept { return "%f"; }
};

template<>
struct InternalPrintFormat<wchar_t, float>
{
    typedef float numeric_type;    // data type being converted to string.
    typedef wchar_t char_type; // char type for target conversion.

    /// Promotion type for casting in argument list of printf.
    typedef double promotion_type;

    static char_type GetFormatChar() noexcept { return L'f'; }
    static constexpr char_type const* GetLengthString() noexcept { return L""; }
    static constexpr char_type const* GetFormatString() noexcept { return L"%f"; }
};

template<>
struct InternalPrintFormat<char, double>
{
    typedef char char_type;      // char type for target conversion.
    typedef double numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef double promotion_type;

    static char_type GetFormatChar() noexcept { return 'f'; }
    static constexpr char_type const* GetLengthString() noexcept { return ""; }
    static constexpr char_type const* GetFormatString() noexcept { return "%f"; }
};

template<>
struct InternalPrintFormat<wchar_t, double>
{
    typedef wchar_t char_type;   // char type for target conversion.
    typedef double numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef double promotion_type;

    static char_type GetFormatChar() noexcept { return L'f'; }
    static constexpr char_type const* GetLengthString() noexcept { return L""; }
    static constexpr char_type const* GetFormatString() noexcept { return L"%f"; }
};

template<>
struct InternalPrintFormat<char, long double>
{
    typedef char char_type;           // char type for target conversion.
    typedef long double numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef long double promotion_type;

    static char_type GetFormatChar() noexcept { return 'f'; }
    static constexpr char_type const* GetLengthString() noexcept { return "L"; }
    static constexpr char_type const* GetFormatString() noexcept { return "%Lf"; }
};

template<>
struct InternalPrintFormat<wchar_t, long double>
{
    typedef wchar_t char_type;        // char type for target conversion.
    typedef long double numeric_type; // data type being converted to string.

    /// Promotion type for casting in argument list of printf.
    typedef long double promotion_type;

    static char_type GetFormatChar() noexcept { return L'f'; }
    static constexpr char_type const* GetLengthString() noexcept { return L"L"; }
    static constexpr char_type const* GetFormatString() noexcept { return L"%Lf"; }
};

} // namespace ocl

#endif // OCL_GUARD_TOSTRING_INTERNAL_INTERNALPRINTFFORMAT_HPP
