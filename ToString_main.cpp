/*
Copyright 2018 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "StringUtility.hpp"
#include <cstddef>
#include <cstdio>
#include <cstdlib>

int main()
{
    typedef char char_type;
    typedef signed char numeric_type;

    std::size_t const str_size = 100;
    char_type str[str_size];
    numeric_type value = -128;

    if (ocl::StringUtility<char_type, numeric_type>::ToString(str, str_size, value))
        printf("%s\n", str);
    else
        printf("Failed to convert %d to string\n", static_cast<int>(value));
}